# .NET SFTP

## Descrição

POC para transferência de arquivos entre servidores.

## Funcionalidade

**Classe Program**
Tem a função de apresentar um menu no console do usuário com três opções de escolha de diretórios onde os arquivos estarão prontos para envio, criar uma instância do objeto de log, guardar algumas informações importantes como o IP do servidor de destino, impressão digital da chave pública do servidor de destino, caminho para chave privada do servidor de origem, caminho para criação de arquivo de log de todo o processo e o método Main() que irá chamar a classe TransferFiles para iniciar a transferência. 
Então basicamente toda a lógica da transferência está encapsulada no método StartTransfer da classe TransferFiles.

`TransferFiles.StartTransfer(sourcePath, archivationPath, remotePath, logger, FileTransferFactory.CreateFileTransfer(), _server, _serviceAccount, _privateKeyPath, _remoteKeyFringerprint)`

Este método precisará dos seguintes parâmetros:
•	Caminho de origem dos arquivos
•	Caminho para arquivamento dos arquivos no servidor de origem
•	Caminho para transferência no servidor de destino
•	Instância do logger
•	FileTransferFactory.CreateFileTransfer(): Este parâmetro solicita uma instância do FileTransferFactory que tem a função de detectar o sistema operacional na origem e criar uma instância do objeto de transferência necessário de acordo esse sistema (SftpransferFromWindows ou SftpransferFromLinux).
•	IP do servidor
•	Conta de usuário
•	Caminho para chave privada
•	Impressão digital da chave pública do destino.

**Classe TransferFiles**
Tem a responsabilidade de chamar a classe VerifyFilesAndDirectories que é a classe que verifica se existem arquivos do tipo .xlsx no diretório de origem e retorna uma lista dos arquivos a serem transferidos.
Caso existam arquivos para transferência, inicia o processo chamando o método Transfer(), neste caso vamos considerar que o sistema de origem seja Windows, então o objeto que chamará esse método é do tipo SftpTransferFromWindows.



**Classe SftpTransferFromWindows**
Esta classe tem a responsabilidade de criar uma sessão de transferência com o servidor de destino, neste caso estamos utilizando uma sessão do tipo SFTP, uma vez que a sessão é estabelecida e o arquivo é enviado, é feita uma comparação do hash do arquivo na origem com o hash do arquivo no destino, caso eles sejam iguais, o processo continua e os arquivos transferidos serão movidos para o diretório de arquivamento no servidor de origem, para isso a classe MoveFilesAndDirectories é utilizada.

Obs.: Foi considerado que junto com os arquivos xlsx haverá um arquivo de log de transmissão log_transmission_file.txt que é gerado junto com os arquivos xlsx, esse log também é enviado junto com os arquivos.