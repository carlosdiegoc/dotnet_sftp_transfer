using sftp_transfer;

namespace application
{
    public class Program
    {
        private static string _serviceAccount = "ubuntu";
        private static string _privateKeyPath = _privateKeyPath = @"C:\workspace\notebook.ppk";
        private static string _remoteKeyFringerprint = "gpS9CieflHFJSEgCbC1Ypc7lJQiGzHK17UYpQS/TTew";
        private static string _server = "54.197.76.218";
        protected static readonly string _logfile = $@"C:\workspace\application\{DateTime.Now:yyyy-MM-dd}-file_transfer_feed.log";       
        static void Main()
        {         
            SerilogConfigurator serilog = new SerilogConfigurator(_logfile);
            var logger = serilog.CreateLogger<Program>();

            var options = new Dictionary<int, Tuple<string, string, string>>
            {
                { 1, new Tuple<string, string, string>(@"C:\workspace\application\Client", @"C:\workspace\application\Archive_Client", "/home/ubuntu/Client") },
                { 2, new Tuple<string, string, string>(@"C:\workspace\application\Position", @"C:\workspace\application\Archive_Position", "/home/ubuntu/Position") },
                { 3, new Tuple<string, string, string>(@"C:\workspace\application\Trade", @"C:\workspace\application\Archive_Trade", "/home/ubuntu/Trade") }
            };

            Console.WriteLine(@"Starting the transfer process... Choose an option below:
                For application Clients, choose 1.
                For application Trade, choose 2.
                For application Position, choose 3.
                To cancel, choose 0.");

            int application;
            while (!int.TryParse(Console.ReadLine(), out application))
            {
                Console.WriteLine("Invalid option. Please enter a valid option: ");
            }

            if (application == 0)
            {
                Console.WriteLine("Process canceled by user.");
                return;
            }
            else if (!options.ContainsKey(application))
            {
                Console.WriteLine("You have chosen an invalid option. Restart the process.");
                return;
            }

            var sourcePath = options[application].Item1;
            var archivationPath = options[application].Item2;
            var remotePath = options[application].Item3;

            TransferFiles.StartTransfer(sourcePath, archivationPath, remotePath, logger, FileTransferFactory.CreateFileTransfer(), _server, _serviceAccount, _privateKeyPath, _remoteKeyFringerprint);
            serilog.Dispose();
        }
    }
}