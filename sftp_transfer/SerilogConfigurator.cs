using Serilog;
using Microsoft.Extensions.Logging;

namespace sftp_transfer
{
    public class SerilogConfigurator
    {
        private ILoggerFactory _loggerFactory;
        public SerilogConfigurator(string LOGFILE)
        {
            var loggerConfiguration = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.Console()
                .WriteTo.File(LOGFILE, rollingInterval: RollingInterval.Day);

            _loggerFactory = new LoggerFactory();
            _loggerFactory.AddSerilog(loggerConfiguration.CreateLogger());
        }
        public Microsoft.Extensions.Logging.ILogger CreateLogger<T>()
        {
            return _loggerFactory.CreateLogger<T>();
        }

        public void Dispose()
        {
            _loggerFactory.Dispose();
        }
    }
}