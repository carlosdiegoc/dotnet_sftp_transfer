using Microsoft.Extensions.Logging;
using sftp_transfer.Interfaces;

namespace sftp_transfer
{
    public class TransferFiles
    {
        public static void StartTransfer(string sourcePath, string archivationPath, string remotePath, ILogger logger, IFileTransfer fileTransfer, string SERVER, string SERVICE_ACCOUNT, string PRIVATE_KEY_PATH, string? SSH_HOST_KEY_FINGERPRINT)
        {
            var files = VerifyFilesAndDirectories.VerifyXlsFiles(sourcePath, logger);

            if (files is not null)
            {
                logger.LogInformation($"Starting to transfer files from {sourcePath} local to {remotePath} remote.");
                bool successfullyMoved = false;
                
                foreach (var file in files)
                {
                    var completeFilePath = Path.Combine(sourcePath, file);
                    var completeRemoteFilePath = $"{remotePath}/{file}";
                    var successfullyTransfered = fileTransfer.Transfer(completeFilePath, completeRemoteFilePath, logger, SERVER, SERVICE_ACCOUNT, PRIVATE_KEY_PATH, SSH_HOST_KEY_FINGERPRINT);

                    if(successfullyTransfered)
                    {
                        logger.LogInformation($"The file {completeFilePath} have been transferred successfully!");
                        logger.LogInformation($"Starting to move file from {completeFilePath} to {Path.Combine(archivationPath, file)}.");
                        successfullyMoved = MoveFilesAndDirectories.MoveFiles(completeFilePath, archivationPath, logger);
                    }
                    else logger.LogInformation("The file can't be transferred and will not be moved.");                                     
                }

                var logTransmissionPath = Path.Combine(sourcePath, "log_transmission_file.txt");
                
                try
                {
                    if(successfullyMoved)
                    {
                        File.Move(logTransmissionPath, Path.Combine(archivationPath, $"{DateTime.Now:yyyy-MM-dd-HH-mm-ss}-{Path.GetFileName(logTransmissionPath)}"));
                        logger.LogInformation("Log transmission file moved successfully!");
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError($"Log transmission file cannot be moved. Error: {ex.Message}");
                    return;
                }
            }
            else
            {
                logger.LogInformation("There is no file to be transferred.");
                return;
            }
        }
    }
}