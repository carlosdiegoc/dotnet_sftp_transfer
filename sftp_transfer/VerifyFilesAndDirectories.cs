using Microsoft.Extensions.Logging;

namespace sftp_transfer
{
    public class VerifyFilesAndDirectories
    {
        public static List<string>? VerifyXlsFiles(string sourcePath, ILogger logger)
        {
            var files = Directory.GetFiles(sourcePath, "*.xlsx").ToList();
            var file_names = new List<string>();
            if (files.Any())
            {
                logger.LogInformation("Files to be transferred:");
                foreach (var file in files)
                {
                    logger.LogInformation(file);
                    file_names.Add(Path.GetFileName(file));
                }
                return file_names;
            }
            else
            {
                logger.LogInformation("No .xls files found.");
                return null;
            }
        }
    }
}