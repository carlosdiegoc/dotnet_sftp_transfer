using Microsoft.Extensions.Logging;

namespace sftp_transfer.Interfaces
{
    public interface IFileTransfer
    {
        bool Transfer(string sourcePath, string remotePath, ILogger logger, string SERVER, string SERVICE_ACCOUNT, string PRIVATE_KEY_PATH, string? SSH_HOST_KEY_FINGERPRINT);
    }
}