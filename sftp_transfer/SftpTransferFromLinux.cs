using System.Diagnostics;
using sftp_transfer.Interfaces;
using Microsoft.Extensions.Logging;

namespace sftp_transfer
{
    public class SftpTransferFromLinux : IFileTransfer
    {
        public bool Transfer(string sourcePath, string remotePath, ILogger logger, string SERVER, string SERVICE_ACCOUNT, string PRIVATE_KEY_PATH, string? SSH_HOST_KEY_FINGERPRINT)
        {
            var commandLinux = $"scp -i {PRIVATE_KEY_PATH} -p {sourcePath} {SERVICE_ACCOUNT}@{SERVER}:{remotePath}";
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{commandLinux}\"",
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };

            try
            {
                process.Start();
                process.WaitForExit();

                if (process.ExitCode == 0)
                {
                    logger.LogInformation($"{sourcePath} transferred successfully!");
                    return true;
                }
                else
                {
                    logger.LogInformation($"File cannot be transferred. Error: {process.StandardError.ReadToEnd()}");
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"An error occurred during file transfer: {ex.Message}");
                return false;
            }
        }
    }
}