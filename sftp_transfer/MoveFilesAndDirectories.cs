using Microsoft.Extensions.Logging;

namespace sftp_transfer
{
    public class MoveFilesAndDirectories
    {
        public static bool MoveFiles(string sourcePath, string archivationPath, ILogger logger)
        {
            try
            {
                File.Move(sourcePath, Path.Combine(archivationPath, Path.GetFileName(sourcePath)));
                logger.LogInformation("File moved successfully!");
                return true;
            }
            catch (Exception ex)
            {
                logger.LogError($"File cannot be moved. Error: {ex.Message}");
                return false;
            }
        }
    }
}