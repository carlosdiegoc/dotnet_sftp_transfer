using sftp_transfer.Interfaces;

namespace sftp_transfer
{
    public class FileTransferFactory
    {
        public static IFileTransfer CreateFileTransfer()
        {
            var osVersion = Environment.OSVersion;
            if (osVersion.Platform == PlatformID.Win32NT)
            {
                return new SftpTransferFromWindows();
            }
            else if (osVersion.Platform == PlatformID.Unix)
            {
                return new SftpTransferFromLinux();
            }
            else
            {
                throw new NotSupportedException("Can not recoginize operational system.");
            }
        }
    }
}