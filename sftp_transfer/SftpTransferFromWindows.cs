using sftp_transfer.Interfaces;
using Microsoft.Extensions.Logging;
using WinSCP;
using System.Security.Cryptography;

namespace sftp_transfer
{
    public class SftpTransferFromWindows : IFileTransfer
    {
        public bool Transfer(string sourcePath, string remotePath, ILogger logger, string SERVER, string SERVICE_ACCOUNT, string PRIVATE_KEY_PATH, string? SSH_HOST_KEY_FINGERPRINT)
        {
            SessionOptions sessionOptions = new()
            {
                Protocol = Protocol.Sftp,
                HostName = SERVER,
                UserName = SERVICE_ACCOUNT,
                SshPrivateKeyPath = PRIVATE_KEY_PATH,
                SshHostKeyFingerprint = SSH_HOST_KEY_FINGERPRINT
            };

            TransferOptions transferOptions = new();
            transferOptions.ResumeSupport.State = TransferResumeSupportState.On;
            transferOptions.PreserveTimestamp = true;

            try
            {
                using Session session = new();

                session.Open(sessionOptions);

                TransferOperationResult transferResult = session.PutFiles(sourcePath, remotePath, false, transferOptions);
                transferResult.Check();

                if (transferResult.IsSuccess)
                {
                    var sha = SHA256.Create();
                    using var localStream = File.OpenRead(sourcePath);
                    var localChecksum = BitConverter.ToString(sha.ComputeHash(localStream)).Replace("-", "").ToLowerInvariant();
                    var destinationHash = GetRemoteFileHash(session, remotePath);
                    if (localChecksum == destinationHash)
                    {
                        logger.LogInformation("The hashes match at the source and the destination, proceeding with the archiving process...");
                        return true;
                    }
                    else
                    {
                        logger.LogError("The hashes don't match at the source and the destination, the process can't continue, please try again...");
                        return false;
                    }
                }
                else
                {
                    logger.LogError("An error occurred during file transfer.");
                    return false;
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"An error occurred during file transfer: {ex.Message}");
                return false;
            }
        }
        private static string GetRemoteFileHash(Session session, string remoteFilePath)
        {
            var remoteChecksumBytes = session.CalculateFileChecksum("sha256", remoteFilePath);
            return BitConverter.ToString(remoteChecksumBytes).Replace("-", "").ToLowerInvariant();
        }
    }
}